<?php

return [

    'reset' => 'Tu contraseña ha sido restablecida!',
    'sent' => 'Hemos enviado su enlace de restablecimiento de contraseña por correo electrónico!',
    'throttled' => 'Por favor espere antes de volver a intentarlo.',
    'token' => 'Este token de restablecimiento de contraseña no es válido.',
    'user' => "Correo electrónico incorrecto.",

];
