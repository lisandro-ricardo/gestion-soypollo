<?php

return [
    'failed' => 'Email o password incorrecta.',
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.'
];
