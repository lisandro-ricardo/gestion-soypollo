<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === config('app.name'))
<img src="{{ asset('storage/soypolloicon.png') }}" class="logo" alt="gestion Soy Pollo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
